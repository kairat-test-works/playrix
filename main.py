import os
import sys
import configparser
import json
from datetime import datetime, timedelta

from core import utils, request


BASE_DIR = os.path.dirname(os.path.abspath(__file__))
CONFIG_FILE = 'config.ini'
config = configparser.ConfigParser()


def save_config(_config, filename=CONFIG_FILE):
    with open(filename, 'w+') as configfile:
        _config.write(configfile)

def find_next(link):
    for l in link.split(','):
        a, b = l.split(';')
        if b.strip() == 'rel="next"':
            return a.strip()[1:-1]

def count_old_ones(items=[], days=0):
    result = 0
    for item in items:
        if item.get('created_at') is None:
            continue
        created_date = datetime.strptime(item.get('created_at'), '%Y-%m-%dT%H:%M:%SZ')
        diff_days = (datetime.now() - created_date).days
        if diff_days >= days:
            result += 1
    return result

def new_line():
    print('\n')

if not os.path.isfile(CONFIG_FILE):
    config.add_section('auth')
    save_config(config)
else:
    config.read(CONFIG_FILE)

def run_program():
    case = 1
    while True:
        if case == 1:
            token = config.get('auth', 'token', fallback='')
            if len(token) != 40:
                utils.print_('Invalid token', ptype='fail')
                token = input('Entesr your valid PAT(Personal access token): ')
                config.set('auth', 'token', token)
                save_config(config)
            else:
                case = 2
        elif case == 2:
            username = config.get('auth', 'username', fallback=None)
            if not username:
                utils.print_('Username not specified', ptype='fail')
                username = input('Enter your github username: ')
                config.set('auth', 'username', username)
                save_config(config)
            else:
                case = 3
        elif case == 3:
            owner = input('Enter repo owner (login): ')
            if len(owner) > 0:
                case = 4
            else:   
                utils.print_('repo owner name cannot be empty!', ptype='fail')
        elif case == 4:
            reponame = input('Enter repo name: ')
            if len(reponame) > 0:
                case = 5
            else:   
                utils.print_('repo name cannot be empty!', ptype='fail')
        elif case == 5:
            _since = input('Enter since datetime (format YYYY-MM-DD): ')
            since = utils.to_datetime(_since, _format='%Y-%m-%d') or datetime(1970, 1, 1)
            case = 6      
        elif case == 6:
            _until = input('Enter until datetime (format YYYY-MM-DD): ')
            until = utils.to_datetime(_until, _format='%Y-%m-%d') or datetime(2970, 12, 31)
            case = 7
        elif case == 7:
            branch = input('Enter branch name: ')
            if len(branch) == 0:
                branch = 'master'
            case = 8
        elif case == 8:
            since = utils.to_iso8601(since)
            until = utils.to_iso8601(until)
            period = '%s..%s' % (since, until)

            new_line()
            utils.print_('PAT: %s' % token, ptype='okblue')
            utils.print_('Searching with user (user agent): %s' % username, ptype='okblue')
            utils.print_('Repo owner: %s' % owner, ptype='okblue')
            utils.print_('Repo name: %s' % reponame, ptype='okblue')
            utils.print_('Search since: %s' % since, ptype='okblue')
            utils.print_('Search until: %s' % until, ptype='okblue')
            utils.print_('Branch: %s' % branch, ptype='okblue')
            new_line()

            try:
                headers = {
                    "Authorization": "token %s" % token,
                    "User-Agent": "%s" % username
                }
                url_params = {
                    'base_url': 'https://api.github.com',
                    'owner': owner,
                    'reponame': reponame
                }

                # commits
                print('Commits')
                url = "%(base_url)s/repos/%(owner)s/%(reponame)s/contributors" % url_params
                with request.http("GET", url, headers=headers) as response:
                    result = json.loads(response.read().decode("utf-8"))
                    contribs = [(_contrib.get('login'), _contrib.get('contributions')) for _contrib in result]
                    contribs.sort(key=lambda x: x[1], reverse=True)
                    for contrib in contribs:
                        print('| %-20s | %10s |' % contrib)
                
                new_line()

                # pulls
                print('Pull requests')
                query_params = request.encode_query_params(**{
                    'q': 'repo:%s type:pr is:open base:%s created:%s' % ('/'.join([owner, reponame]), branch, period)
                })
                url_params['q'] = query_params
                url = "%(base_url)s/search/issues?%(q)s" % url_params
                with request.http("GET", url, headers=headers) as response:
                    result = json.loads(response.read().decode("utf-8"))
                    total_count = result.get('total_count', 0)
                    print('| %-20s | %10s |' % ('open pull requests', total_count))

                old_pull_count = count_old_ones(items=result.get('items', []), days=30)
                next_url = find_next(response.getheader('Link')) if response.getheader('Link') else None
                if next_url:
                    while True:
                        with request.http('GET', next_url, headers=headers) as resp:
                            _result = json.loads(resp.read().decode("utf-8"))
                            old_pull_count += count_old_ones(items=_result.get('items', []), days=30)
                            next_url = find_next(resp.getheader('Link')) if resp.getheader('Link') else None                     
                            if next_url is None:
                                break
                print('| %-20s | %10s |' % ('old pull requests', old_pull_count))


                query_params = request.encode_query_params(**{
                    'q': 'repo:%s type:pr is:closed base:%s closed:%s' % ('/'.join([owner, reponame]), branch, period)
                })
                url_params['q'] = query_params
                url = "%(base_url)s/search/issues?%(q)s" % url_params
                with request.http("GET", url, headers=headers) as response:
                    result = json.loads(response.read().decode("utf-8"))
                    print('| %-20s | %10s |' % ('closed pull requests', result.get('total_count', 0)))
                
                new_line()

                #issues
                print('Issues')
                query_params = request.encode_query_params(**{
                    'q': 'repo:%s is:issue is:open created:%s' % ('/'.join([owner, reponame]), period)
                })
                url_params['q'] = query_params
                url = "%(base_url)s/search/issues?%(q)s" % url_params
                with request.http("GET", url, headers=headers) as response:
                    result = json.loads(response.read().decode("utf-8"))
                    print('| %-20s | %10s |' % ('open issues', result.get('total_count', 0)))

                old_issue_count = count_old_ones(items=result.get('items', []), days=14)
                next_url = find_next(response.getheader('Link')) if response.getheader('Link') else None
                if next_url:
                    while True:
                        with request.http('GET', next_url, headers=headers) as resp:
                            _result = json.loads(resp.read().decode("utf-8"))
                            old_issue_count += count_old_ones(items=_result.get('items', []), days=14)
                            next_url = find_next(resp.getheader('Link')) if resp.getheader('Link') else None
                            if next_url is None:
                                break
                print('| %-20s | %10s |' % ('old issues', old_issue_count))
                
                query_params = request.encode_query_params(**{
                    'q': 'repo:%s is:issue is:closed closed:%s' % ('/'.join([owner, reponame]), period)
                })
                url_params['q'] = query_params
                url = "%(base_url)s/search/issues?%(q)s" % url_params
                with request.http("GET", url, headers=headers) as response:
                    result = json.loads(response.read().decode("utf-8"))
                    print('| %-20s | %10s |' % ('closed issues', result.get('total_count', 0)))

            except Exception as err:
                utils.print_(str(err), ptype='fail')

            case = 9
        elif case == 9:
            val = input('Again? (y/n): ')
            case = 1 if val == 'y' else 10
        else:
            break



if __name__ == '__main__':
    run_program()



