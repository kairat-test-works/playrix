from datetime import datetime


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def print_(text='', ptype=None, _bcolors=bcolors, **kwargs):
    if ptype == 'warning':
        text = _bcolors.WARNING + text + _bcolors.ENDC
    elif ptype == 'fail':
        text = _bcolors.FAIL + text + _bcolors.ENDC
    elif ptype == 'header':
        text = _bcolors.HEADER + text + _bcolors.ENDC
    elif ptype == 'bold':
        text = _bcolors.BOLD + text + _bcolors.ENDC
    elif ptype == 'okblue':
        text = _bcolors.OKBLUE + text + _bcolors.ENDC
    elif ptype == 'okgreen':
        text = _bcolors.OKGREEN + text + _bcolors.ENDC
    print(text, **kwargs)

def to_iso8601(when=None):
    if not when:
        when = datetime.now()
    return when.strftime("%Y-%m-%dT%H:%M:%SZ")

def to_datetime(dtstr, _format='%Y-%m-%d %H:%M:%S'):
    try:
        dt = datetime.strptime(dtstr, _format)
    except ValueError:
        dt = None
    return dt

